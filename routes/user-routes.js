const express = require("express");
const router = express.Router();
const db = require("../models");

router.post("/new", function (req, res) {
    db.User.create({
        username: req.body.username
    }).then(function (newUser) {
        res.status(200).json({
            status: 1,
            message: "Created User successfully"
        })
    })
})


router.get("/all", function (req, res) {
    db.User.findAll({
        include: [db.Profile, db.Post]
    }).then(allUsers => res.send(allUsers))
})

module.exports = router