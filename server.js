const express = require("express")

const app = express();
const db = require("./models");
const userRoutes = require("./routes/user-routes")
const profileRoutes = require("./routes/profile-route")
const postRoutes = require("./routes/profile-route")
const PORT = process.env.PORT || 7070;

app.use(express.urlencoded({ extended: true }))
app.use(express.json())


app.use("/api/users", userRoutes)
// app.use("/api/post", postRoutes)
// app.use("/api/profile", profileRoutes)

db.sequelize.sync().then(() => {
    app.listen(PORT, () => {
        console.log(`listenning at: httpL//localhost:${PORT}`)
    })
})