
module.exports = function (sequelize, DataTypes) {
    const Profile = sequelize.define("Profile", {
        username: {
            type: DataTypes.STRING,
            allowNull: false
        }
    })

    Profile.associate = function (models) {
        Profile.belongsTo(models.User, {
            foreignKey: {
                allowNull: false
            }
        })
    }

    return Profile;
}